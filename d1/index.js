alert("hello,batch241")


// Arithmetic Operators

let x = 1397;
let y = 7831;

//Addition Operator

let sum = x + y;
console.log("Result of addition operator: " + sum);

//Subtraction Operator

let difference = x - y;
console.log("Result of the subtraction operator: " + difference);

//Multiplication Operator
let product = x * y;
console.log("Result of the multiplication of the produc: " + product);

//Division Operator
let	 quotient = x / y;
console.log("Result of the division operator: " + quotient);


//Modulo Operator

let remainder = y % x;
console.log("Result of the modulo operator: " + remainder);

//Assignment Operator
//Basic Assignment Operator(=)
//The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;


//Addition Assignment Operator
//Uses the current value of the variable and it ADDS a number(2) to itself.
//Afterwards, reassigns it as a new value.
assignmentNumber += 2;
console.log("Result of the addition assignment Operator: " + assignmentNumber);

//Subtraction/Multiplication/Division(-=, *=, /=);


//Subtaction Assignment operator
assignmentNumber -= 2;
console.log("Result of the Subtraction Assignment Operator: " + assignmentNumber);


//Multiplication Assignment Operator

assignmentNumber *= 2;
console.log("Result of the Subtraction Assignment Operator: " + assignmentNumber);

//Division Assignment Operator 
assignmentNumber /= 2;
console.log("Result of the division assignment operator: " + assignmentNumber); 


//Multiple Operators and Parentheses
/*
when multiple operators are applied in a single statement, it follows the PEMDAS rule.
(Parentheses,Exponents,Multiplication,Division,Addition, and Subtraction);
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3.1 + 2 = 3
	4. 3 - 2.4 = 0.6

*/
// let mdas = 3 - 2.4;


let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);//.6

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas); //.2


let sample = 5 % 2 + 10;
/*
	5 % 2 = 1
	1 + 10 = 11


*/
console.log(sample);

sample = 5 + 2 % 10;
console.log(sample);

//INCREMENT AND DECREMENT
//Operators that add or substract values by 1 and reassigns the value
// of the variable where the increment/decrement was applied to

let z = 1;


//Pre-Increment

//The value of "z" is added by a value of 1 before returning the value and storing 
//it in the variable "increment";
// let preIncrement = ++z;

// console.log("Result of the pre-increment operation: " + preIncrement);

// console.log("Result of the pre-increment: " + z);

//Post-Increment
// The value of "z" is returned and stored in the 
// variable "postIncrement" then the value of "z" is increased by 1.
let postIncrement = z++;

console.log("Result of the postIncrement: " + postIncrement);
console.log("Result of the postIncrement: " + z);

/*

	Pre-increment - adds 1 first before reading the value 
	Post - increment - reads the value first before adding  1
	
	Pre-decremnt - subtracts 1 first before reading the value 
	Post - decrement - reads the value first before subtracting  1
	
*/

//Pre-Decrement
//The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecement"

let a = 2;

// let preDecrement = --a;
// console.log("Result of the pre-decrement: " + preDecrement);
// console.log("Result of the pre-decrement: " + a);

//Post-Decrement
// The value "a" is returned and stored in the variable "postDecrement" then the value of "a" is decreased by 1;
let postDecrement = a--;
console.log("Result of the postDecremet: " + postDecrement);
console.log("Result of the postDecrement: " + a) ;

//TYPE COERCION

/*
	Type coercion is the automatic or implicit conversion of values
	from one data type to another

*/


let numA = '10';

let numB = 12;
/*
	
	Adding/concatenating a string and a number will result
	as a string

*/

let coercion1 = numA + numB;
console.log(coercion1);
console.log(typeof coercion1);




let coercion = numA - numB;
console.log(coercion);
console.log(typeof coercion);

//Non-coercion

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


//Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with
	the value of 1



*/
let numE = true + 1;
console.log(numE);
console.log(typeof numE); 

let numF = false + 1;
console.log(numF);
console.log(typeof numF);

//Comparison Operator

let juan = 'juan';

//Equality Operator(==)

/*
	-checks whether the operand are equal
	or have the same content
	-Attempts to CONVERT and COMPARE operands of different
	data types.
	-returns a boolean value(true / false)

*/

console.log(1 == 1);// T
console.log(1 == 2);// F
console.log(1 == '1');// T
console.log(1 == true);// T

//compare two strings that are the same

console.log('juan' == 'juan'); 
console.log ('true' == true);
console.log (juan == 'juan');

//Inequality Operator(!=)

/*
	-checks whether the operands are not equal/have different
     content
     - Attempts to CONVERT AND COMPARE operands of diff data types.

*/


console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(1 != true);
console.log('juan' != 'juan');
console.log ('juan' != juan);

//Strict Equality Operator(===)

/*
	-Checks whether the  operands are equal
	or have the same content 
	- Also COMPARES the data types of 2 values
*/

console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false
console.log(1 === true);//false
console.log('juan' === 'juan');//true
console.log(juan === 'juan');//true

//Strict Inequality operator(!==)

/*
	-Checks whether the operands are not equal
	or have the same content and also compares the
	data types of 2 values.
*/
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(1 !== true);//true
console.log('juan' !== 'juan');//false
console.log(juan !== 'juan');//false

//RELATIONAL OPERATOR
// Returns a boolean value
let j = 50;
let k = 65;

//GT/Greater than operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan);

//LT/Less than operator (<)
let isLessThan = j < k;
console.log(isLessThan);

//GTE /Greater than or equal operator(>=)

let isGTorEqual = j >= k;
console.log(isGTorEqual);

//LTE /less than or equal operator(<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual);



let numStr = "30";
//forced coercion to change the string to a number.
console.log(j > numStr);

let str = "thirty";
console.log(j > str);


//Logical Operator

let isLegalAge = true;
let isRegistered = false;

//Logical AND operator(&& -double Ampersand)
//Returns true if all operands are true
//true && true = true
//true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

//Logical OR operator(|| - double pipe)
//Returns true if one of the operands are true
//true || true = true
// true || false = true
// false  || false = false
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet);

//Logical NOT Operator (! - exclamation point)

let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet);